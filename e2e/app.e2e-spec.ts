import { FavogitPage } from './app.po';

describe('favogit App', () => {
  let page: FavogitPage;

  beforeEach(() => {
    page = new FavogitPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
