export class Repos {
    id: string;
    fullname: string;
    description: string;
    url: string;
}
