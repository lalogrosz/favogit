import { Repos } from './repos';
export class User {
    id: string;
    avatar: string;
    username: string;
    name: string;
    url: string;
    profile: string;
    reposUrl: string;
    location: string;
    email: string;
    date: string;
    following: string;
    followers: string;
    isFavourite: boolean;
    createdAt: string;
    repos: Repos[];
}
