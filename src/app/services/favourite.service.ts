import { User } from './../models/user';
import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';


@Injectable()
export class FavouriteService {

    public favourites: User[] = [];

    constructor(private local: LocalStorageService) {
        this.favourites = local.retrieve('favourites');
    }

    public add(user: User) {
        user.isFavourite = true;
        this.favourites.push(user);
        this.updateStorage();
    }

    public remove(user: User) {
        user.isFavourite = false;
        const index = this.favourites.findIndex(item => item.id === user.id);
        if (index !== -1) {
            this.favourites = [
                ...this.favourites.slice(0, index),
                ...this.favourites.slice(index + 1)
            ];
            this.updateStorage();
        }
    }

    protected updateStorage() {
        this.local.store('favourites', this.favourites);
    }

    public toggleFav(user: User) {
        if (!user.isFavourite) {
            this.add(user);
        } else {
            this.remove(user);
        }
    }


}
