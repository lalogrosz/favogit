import { Repos } from './../models/repos';
import { User } from './../models/user';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SearchService {

  public order = '-username';
  public criteria = '';
  public inList = false;

  constructor(protected http: Http) { }

  filter() {
    const searchTerm = `${this.criteria} in:username ${this.criteria} in:fullname`;
    return this.http.get(`${environment.github_api}/search/users?q=${searchTerm}`)
        .map(res => res.json());
  }

  oneUserByUsername(username): Observable<User> {
    return this.http.get(`${environment.github_api}/users/${username}`)
        .map(res => {
          return this.toModel(res);
        });
  }

  getRepos(url): Observable<Repos[]> {
    return this.http.get(url)
        .map(res => {
          let repo;
          const repos: Repos[] = [];
          for (repo of res.json()) {
            const reposModel: Repos = {
              id: repo.id,
              fullname: repo.full_name,
              description: repo.description,
              url: res.url
            };

            repos.push(reposModel);
          }
          return repos;
        });
  }

  protected toModel(res) {
    const user = res.json();
    const userModel: User = {
      id: user.id,
      avatar: user.avatar_url,
      username: user.login,
      name: user.name,
      url: user.url,
      profile: user.html_url,
      reposUrl: user.repos_url,
      location: user.location,
      email: user.email,
      date: user.created_at,
      following: user.following,
      followers: user.followers,
      isFavourite: false,
      createdAt: user.created_at,
      repos: []
    };
    return userModel;
  }
}
