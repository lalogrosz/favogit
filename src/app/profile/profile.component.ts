import { Repos } from './../models/repos';
import { User } from './../models/user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FavouriteService } from './../services/favourite.service';
import { SearchService } from './../services/search.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public user: User;

  constructor(
    private route: ActivatedRoute,
    public favouriteService: FavouriteService,
    protected service: SearchService) { }

  ngOnInit() {
    // Get the router params
    this.route.params.subscribe(params => {

      const username = params['username'];
      this.service.oneUserByUsername(username).subscribe((userModel: User) => {

        if (this.favouriteService.favourites.find(item => item.id === userModel.id)) {
          userModel.isFavourite = true;
        }
        this.user = userModel;

        this.service.getRepos(userModel.reposUrl).subscribe((repos: Repos[]) => {
          this.user.repos = repos;
        });
      });
    });
  }

}
