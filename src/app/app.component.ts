import { SearchService } from './services/search.service';
import { FavouriteService } from './services/favourite.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private route: Router,
    public searchService: SearchService,
    public favouriteService: FavouriteService) {
  }

  public onEnter() {
    this.searchService.order = '-username';
    this.search();
  }

  public toggleOrder() {
    if ( this.searchService.order === '+username') {
      this.searchService.order = '-username';
    } else {
      this.searchService.order = '+username';
    }
  }

  protected search() {
    this.route.navigate(['/list', this.searchService.criteria]);
  }
}
