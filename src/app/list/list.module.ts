import { FavouriteService } from './../services/favourite.service';
import { SearchService } from './../services/search.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import { NgArrayPipesModule } from 'angular-pipes';

@NgModule({
  imports: [
    CommonModule,
    NgArrayPipesModule
  ],
  declarations: [ListComponent],
  providers: [SearchService, FavouriteService]
})
export class ListModule { }
