import { environment } from './../../environments/environment';
import { FavouriteService } from './../services/favourite.service';
import { SearchService } from './../services/search.service';
import { User } from './../models/user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public users: User[] = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public service: SearchService,
    public favouriteService: FavouriteService,
  ) { }

  ngOnInit() {
    
    // Get the router params
    this.route.params.subscribe(params => {

      // Work arroud for change detection
      setTimeout(() => {
        this.service.criteria = params['criteria'];

        // Search all with the criteria
        this.service.filter().subscribe(list => {
          let user;
          // Limit the results
          const items = list.items.slice(0, environment.max_results);

          this.users = [];
          for (user of items) {
            // For each user, get full information
            this.service.oneUserByUsername(user.login).subscribe((userModel: User) => {
              // Check if is favourite
              if (this.favouriteService.favourites.find(item => item.id === userModel.id)) {
                userModel.isFavourite = true;
              }

              // Append
              this.users.push(userModel);

              // Another WA to display the results at the first search
              setTimeout(() => {
                this.service.order = '+username';
                this.service.inList = true;
              }, 1000);
            });
          }
        });
      }, 100);

    });
  }

  goToProfile(username) {
    this.service.inList = false;
    this.router.navigate(['/profile', username]);
  }

}
